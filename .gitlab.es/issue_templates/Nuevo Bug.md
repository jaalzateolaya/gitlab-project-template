# Descripción del problema

<!-- Describa brevemente qué error se está presentando. -->

## URL

<!-- La dirección (URL) donde se produce el error. Cópiela y péguela tal cual
como está en el navegador. -->

## Pasos para reproducir el error

<!-- Describa los pasos para reproducir el error. -->

1. <!-- Primer paso. -->
2. <!-- Segundo Paso. -->
<!-- n. Enésimo paso. -->

## Resultado actual

<!-- Describa qué resultado le está presentando el estado de error, por ejemplo:

	* El sistema está lanzando un error 500.
	* Aparece un error extraño.
	* El botón no parece responder. -->


<details>
<summary>Error presentado por la aplicación</summary>
<!-- Si el sistema arroja un error, por favor cópielo en esta sección. -->
<pre>

(Copie aquí.)

</pre>
</details>

## Resultado esperado

<!-- Describa cual es el resultado esperado por la acción que se está
presentando error. -->

## Información adicional

### Sesión

<!-- Escriba correo del usuario con el que se está presentando el error -->
Usuario:
<!-- Tiene conocimientos de si el error se reproduce con otros usuarios?
Responda Sí o No -->
Se reproduce con otras cuentas: (Sí/No)

### Evidencias adjuntas

<!-- Inserte aquí las vídeo demostraciones o capturas de pantalla. -->

## Posible causa del problema y solución

<!-- Si cree que conoce una posible solución coméntela aquí. -->

<!-- Por favor no modifique esta sección -->
/label ~New
/label ~Bug
